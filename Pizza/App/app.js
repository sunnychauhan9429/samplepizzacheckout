﻿var pizzaApp = angular.module("pizzaApp", ['ui.router']);

pizzaApp.run(function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    Array.prototype.removeValue = function (name, value) {
        var array = $.map(this, function (v, i) {
            return v[name] === value ? null : v;
        });
        this.length = 0; //clear original array
        this.push.apply(this, array); //push all elements except the one we want to delete
    };
    $rootScope.SelectedBase = [];
    $rootScope.SelectedToppings = [];
    $rootScope.SelectedVeggies = [];
    $rootScope.SelectedCheese = [];
    $rootScope.SelectedSauces = [];
    $rootScope.SelectedSides = [];
    $rootScope.SelectedExtras = [];
    $rootScope.Total = 0;

    ////Populating Data
    $rootScope.Base = [{ id: "base_1", name: "Normal", price: "300" },
                    { id: "base_2", name: "Thin Crust", price: "350" }];
    $rootScope.Toppings = [{ id: "toppings_1", name: "Anchovies", price: "50" },
                            { id: "toppings_2", name: "Bacon", price: "100" },
                            { id: "toppings_3", name: "Canadian Bacon ", price: "150" },
                            { id: "toppings_4", name: "Chicken", price: "100" },
                            { id: "toppings_5", name: "Italian sausage", price: "175" },
                            { id: "toppings_6", name: "Sausage", price: "125" },
                            { id: "toppings_7", name: "Pepperoni", price: "90" }];
    $rootScope.Veggies = [{ id: "veggies_1", name: "Green Peppers", price: "50" },
                          { id: "veggies_2", name: "Mushrooms", price: "25" },
                          { id: "veggies_3", name: "Onion", price: "30" },
                          { id: "veggies_4", name: "Tomatoes", price: "30" },
                          { id: "veggies_5", name: "Banana Peppers", price: "70" },
                          { id: "veggies_6", name: "Pineapple Tidbits", price: "65" },
                          { id: "veggies_7", name: "Ripe Olives", price: "95" },
                          { id: "veggies_8", name: "Green Olives", price: "90" },
                          { id: "veggies_9", name: "Jalapeno Peppers", price: "75" }];
    $rootScope.Cheese = [{ id: "cheese_1", name: "Parmesan/Romano", price: "100" },
                         { id: "cheese_2", name: "Three Cheese Blend", price: "150" }];
    $rootScope.Sauces = [{ id: "sauces_1", name: "Chicken BBQ Pizza Sauce", price: "80" },
                         { id: "sauces_2", name: "Ranch Sauce", price: "70" },
                         { id: "sauces_3", name: "Spinach Alfredo Sauce", price: "75" }];
    $rootScope.SidesandDesserts = [{ id: "sidesanddesserts_1", name: "Chicken Poppers", price: "200" },
                                   { id: "sidesanddesserts_2", name: "Chicken Wings (Roasted)", price: "250" },
                                   { id: "sidesanddesserts_3", name: "Chocolate Chips Cookie", price: "150" },
                                   { id: "sidesanddesserts_4", name: "Double Chocolate Chip Brownie", price: "200" }];
    $rootScope.Extras = [{ id: "extras_1", name: "Pepperoncini", price: "25" },
                        { id: "extras_2", name: "BBQ Dipping Sauce", price: "25" },
                        { id: "extras_3", name: "Blue Cheese Dipping Sauce", price: "25" },
                        { id: "extras_4", name: "Buffalo Dipping Sauce", price: "25" },
                        { id: "extras_5", name: "Cheese Dipping Sauce", price: "25" },
                        { id: "extras_6", name: "Garlic Sauce", price: "25" },
                        { id: "extras_7", name: "Garlic Parmesan Sauce", price: "25" },
                        { id: "extras_8", name: "Honey Chipotle Sauce", price: "25" }];
    $rootScope.EnableDisableButton = function (max) {
        if ($(".selected-item").length >= max)
            $(".btn-primary").removeClass("disabled");
        else
            $(".btn-primary").addClass("disabled");
    };
    $rootScope.getSelectedItem = function ($event) {
        var selectedItem = $event.currentTarget;
        return { name: $(selectedItem).attr("data-name"), id: $(selectedItem).attr("data-id"), price: $(selectedItem).attr("data-price") };
    };
    $rootScope.resetData = function () {
        $rootScope.SelectedBase = [];
        $rootScope.SelectedToppings = [];
        $rootScope.SelectedVeggies = [];
        $rootScope.SelectedCheese = [];
        $rootScope.SelectedSauces = [];
        $rootScope.SelectedSides = [];
        $rootScope.SelectedExtras = [];
        $rootScope.Total = 0;
    }
});

pizzaApp.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'Templates/main.html',
        controller: ("homeCtrl", ['$scope', '$rootScope', '$state'], function ($scope, $rootScope, $state) {
            $rootScope.resetData();
            $scope.startMenu = function () {
                $state.go('base');
            };
        })
    })
    .state('base', {
        url: '/base',
        templateUrl: 'Templates/base.html',
        controller: ("baseCtrl", ['$scope', '$rootScope', '$state'], function ($scope, $rootScope, $state) {
            $scope.nextState = function () {
                $state.go('toppings');
            };
            $scope.prevState = function () {
                $state.go('home');
            };
            $scope.selectBase = function ($event) {
                $(".selected-item").removeClass("selected-item");
                $($event.currentTarget).addClass("selected-item");
                $rootScope.SelectedBase = {
                    id: $($event.currentTarget).attr("data-id"),
                    name: $($event.currentTarget).attr("data-name"),
                    price: $($event.currentTarget).attr("data-price"),
                };
                $rootScope.Total = parseInt($($event.currentTarget).attr("data-price"));
                $rootScope.EnableDisableButton(1);
            };
        })
    })
    .state('toppings', {
        url: '/toppings',
        templateUrl: 'Templates/toppings.html',
        controller: ("toppingsCtrl", ['$scope', '$rootScope', '$state'], function ($scope, $rootScope, $state) {
            $scope.nextState = function () {
                $state.go('veggies');
            };
            $scope.prevState = function () {
                $state.go('base');
            };
            $scope.selectToppings = function ($event) {
                if ($($event.currentTarget).hasClass("selected-item")) {
                    $($event.currentTarget).removeClass("selected-item");
                    $rootScope.SelectedToppings.removeValue('id', $($event.currentTarget).attr("data-id"));
                    $rootScope.Total = $rootScope.Total - parseInt($($event.currentTarget).attr("data-price"));
                }
                else {
                    if ($(".selected-item").length >= 3)
                        return;
                    else {
                        $($event.currentTarget).addClass("selected-item");
                        $rootScope.SelectedToppings.push($rootScope.getSelectedItem($event));
                        $rootScope.Total = $rootScope.Total + parseInt($($event.currentTarget).attr("data-price"));
                    }
                }
                $rootScope.EnableDisableButton(1);                
            };
        })
    })
    .state('veggies', {
        url: '/veggies',
        templateUrl: 'Templates/veggies.html',
        controller: ("veggiesCtrl", ['$scope', '$rootScope', '$state'], function ($scope, $rootScope, $state) {
            $scope.nextState = function () {
                $state.go('cheese');
            };
            $scope.prevState = function () {
                $state.go('toppings');
            };
            $scope.selectVeggies = function ($event) {
                if ($($event.currentTarget).hasClass("selected-item")) {
                    $($event.currentTarget).removeClass("selected-item");
                    $rootScope.SelectedToppings.removeValue('id', $($event.currentTarget).attr("data-id"));
                    $rootScope.Total = $rootScope.Total - parseInt($($event.currentTarget).attr("data-price"));
                }
                else {
                    if ($(".selected-item").length >= 5)
                        return;
                    else {
                        $($event.currentTarget).addClass("selected-item");
                        $rootScope.SelectedVeggies.push($rootScope.getSelectedItem($event));
                        $rootScope.Total = $rootScope.Total + parseInt($($event.currentTarget).attr("data-price"));
                    }
                }
                $rootScope.EnableDisableButton(1);
            };
        })
    })
    .state('cheese', {
        url: '/cheese',
        templateUrl: 'Templates/cheese.html',
        controller: ("cheeseCtrl", ['$scope', '$rootScope', '$state'], function ($scope, $rootScope, $state) {
            $scope.nextState = function () {
                $state.go('sauces');
            };
            $scope.prevState = function () {
                $state.go('veggies');
            };
            $scope.selectCheese = function ($event) {
                if ($(".selected-item").length > 0) {
                    $rootScope.Total = $rootScope.Total - parseInt($($(".selected-item")[0]).attr("data-price"));
                    $(".selected-item").removeClass("selected-item");
                }
                $($event.currentTarget).addClass("selected-item");
                $rootScope.SelectedCheese = {
                    id: $($event.currentTarget).attr("data-id"),
                    name: $($event.currentTarget).attr("data-name"),
                    price: $($event.currentTarget).attr("data-price"),
                };
                $rootScope.Total = $rootScope.Total + parseInt($($(".selected-item")[0]).attr("data-price"));
                $rootScope.EnableDisableButton(1);
            };
        })
    })
    .state('sauces', {
        url: '/sauces',
        templateUrl: 'Templates/sauces.html',
        controller: ("saucesCtrl", ['$scope', '$rootScope', '$state'], function ($scope, $rootScope, $state) {
            $scope.nextState = function () {
                $state.go('sides');
            };
            $scope.prevState = function () {
                $state.go('cheese');
            };
            $scope.selectSauce = function ($event) {
                if ($($event.currentTarget).hasClass("selected-item")) {
                    $($event.currentTarget).removeClass("selected-item");
                    $rootScope.SelectedSauces.removeValue('id', $($event.currentTarget).attr("data-id"));
                    $rootScope.Total = $rootScope.Total - parseInt($($event.currentTarget).attr("data-price"));
                }
                else {
                    $($event.currentTarget).addClass("selected-item");
                    $rootScope.SelectedSauces.push($rootScope.getSelectedItem($event));
                    $rootScope.Total = $rootScope.Total + parseInt($($event.currentTarget).attr("data-price"));

                }
                $rootScope.EnableDisableButton(1);
            };
        })

    })
    .state('sides', {
        url: '/sides',
        templateUrl: 'Templates/sidesAndDeserts.html',
        controller: ("sidesCtrl", ['$scope', '$rootScope', '$state'], function ($scope, $rootScope, $state) {
            $scope.nextState = function () {
                $state.go('extras');
            };
            $scope.prevState = function () {
                $state.go('sauces');
            };
            $scope.selectSides = function ($event) {
                if ($($event.currentTarget).hasClass("selected-item")) {
                    $($event.currentTarget).removeClass("selected-item");
                    $rootScope.SelectedSides.removeValue('id', $($event.currentTarget).attr("data-id"));
                    $rootScope.Total = $rootScope.Total - parseInt($($event.currentTarget).attr("data-price"));
                }
                else {
                    $($event.currentTarget).addClass("selected-item");
                    $rootScope.SelectedSides.push($rootScope.getSelectedItem($event));
                    $rootScope.Total = $rootScope.Total + parseInt($($event.currentTarget).attr("data-price"));
                }
            };
        })
    })
    .state('extras', {
        url: '/extras',
        templateUrl: 'Templates/extras.html',
        controller: ("extrasCtrl", ['$scope', '$rootScope', '$state'], function ($scope, $rootScope, $state) {
            $scope.nextState = function () {
                $state.go('checkout');
            };
            $scope.prevState = function () {
                $state.go('sides');
            };
            $scope.selectExtras = function ($event) {
                if ($($event.currentTarget).hasClass("selected-item")) {
                    $($event.currentTarget).removeClass("selected-item");
                    $rootScope.SelectedExtras.removeValue('id', $($event).attr("data-id"));
                    $rootScope.Total = $rootScope.Total - parseInt($($event.currentTarget).attr("data-price"));
                }
                else {
                    $($event.currentTarget).addClass("selected-item");
                    $rootScope.SelectedExtras.push($rootScope.getSelectedItem($event));
                    $rootScope.Total = $rootScope.Total + parseInt($($event.currentTarget).attr("data-price"));
                }                
            };
        })
    })
    .state('checkout', {
        url: '/checkout',
        templateUrl: 'Templates/checkout.html',
        controller: ("checkoutCtrl", ['$scope', '$rootScope', '$state'], function ($scope, $rootScope, $state) {

            $scope.nextState = function () {
                $state.go('home');
            };
            $scope.prevState = function () {
                $("#alert-area").html(`<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> Order added to cart.
</div>`);
                $state.go('home');
            };
        })
    });
});

